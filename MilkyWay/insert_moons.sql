 /**
  *
  *  SELECT output is value of an INSERT :
  *  Considered repetitive queries.
  *  Ensure SELECT result is not unique.
  *
 **/

/**
 *  Jupiter
**/

INSERT INTO moons (planet_id, name)
VALUES
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Adrastea'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Amalthea'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Callisto'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Carpo'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Europa'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Ganymede'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Io'),
  ( (SELECT planet_id FROM planets WHERE name = 'Jupiter'), 'Thebe');

/**
 *  Saturn
 *  Inefficient - Temp tables
**/

CREATE TABLE IF NOT EXISTS saturn_temp (
  name TEXT,
  planet varchar(12)
 );

INSERT INTO saturn_temp (name, planet)
VALUES
  ('Atlas', 'Saturn'),
  ('Calypso', 'Saturn'),
  ('Daphne', 'Saturn'),
  ('Dione', 'Saturn'),
  ('Enceladus', 'Saturn'),
  ('Fenrir', 'Saturn'),
  ('Helene', 'Saturn'),
  ('Hyperion', 'Saturn'),
  ('Iapetus', 'Saturn'),
  ('Janus', 'Saturn'),
  ('Mimas', 'Saturn'),
  ('Pan', 'Saturn'),
  ('Pandora', 'Saturn'),
  ('Phoebe', 'Saturn'),
  ('Prometheus', 'Saturn'),
  ('Rhea', 'Saturn'),
  ('Tethys', 'Saturn'),
  ('Ymir', 'Saturn');


INSERT INTO moons (planet_id, name) (
  SELECT planets.planet_id, saturn_temp.name FROM planets
   INNER JOIN saturn_temp ON planets.name = saturn_temp.planet
);

DROP TABLE IF EXISTS saturn_temp;

/**
 *  Output the results in the terminal
**/

SELECT planets.name, moons.name FROM planets INNER JOIN moons ON planets.planet_id = moons.planet_id;
