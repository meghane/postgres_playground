# README #

### What is this repository for? ###

* Personal Learning for Relational DB Design and Spring Transaction Management
* Follows Concepts from the Book [Database in Depth](http://www.goodreads.com/book/show/573821.Database_in_Depth) by C.J. Date

### How do I get set up? ###

* Install local postgress
* Set the password:

``` 
sudo -u postgres psql postgres
	
Enter new password: postgres
		
Enter it again: postgres 
		
```

* Execute the included bash script

	`./run.sh`
	
### PSQL Quick Commands ###
* List all databases

	` \list`  or ` \l `

* List all tables in current db

	` \dt ` 

* Connect to Database

	` \connect [database_name]` 
