/*
 * Inner Join
 * http://www.w3schools.com/Sql/sql_join_inner.asp
 * 'INNER JOIN' keyword selects rows from both tables as long as
 *  there is a match between the columns in both tables.
 */

SELECT * FROM planets INNER JOIN moons ON planets.planet_id = moons.planet_id;

/*
 planet_id |  name   | moon_id | planet_id |    name
-----------+---------+---------+-----------+------------
         5 | Jupiter |       8 |         5 | Thebe
         5 | Jupiter |       7 |         5 | Io
         5 | Jupiter |       6 |         5 | Ganymede
         5 | Jupiter |       5 |         5 | Europa
         5 | Jupiter |       4 |         5 | Carpo
         5 | Jupiter |       3 |         5 | Callisto
         5 | Jupiter |       2 |         5 | Amalthea
         5 | Jupiter |       1 |         5 | Adrastea
         6 | Saturn  |      26 |         6 | Ymir
         6 | Saturn  |      25 |         6 | Tethys
         6 | Saturn  |      24 |         6 | Rhea
         6 | Saturn  |      23 |         6 | Prometheus
         6 | Saturn  |      22 |         6 | Phoebe
         6 | Saturn  |      21 |         6 | Pandora
         6 | Saturn  |      20 |         6 | Pan
         6 | Saturn  |      19 |         6 | Mimas
         6 | Saturn  |      18 |         6 | Janus
         6 | Saturn  |      17 |         6 | Iapetus
         6 | Saturn  |      16 |         6 | Hyperion
         6 | Saturn  |      15 |         6 | Helene
         6 | Saturn  |      14 |         6 | Fenrir
         6 | Saturn  |      13 |         6 | Enceladus
         6 | Saturn  |      12 |         6 | Dione
         6 | Saturn  |      11 |         6 | Daphne
         6 | Saturn  |      10 |         6 | Calypso
         6 | Saturn  |       9 |         6 | Atlas
*/

/*
 * Full Outer Join
 * http://www.w3schools.com/Sql/sql_join_full.asp
 * 'FULL OUTER JOIN' keyword returns all rows from both tables regardless of resultsets
 */

SELECT * FROM planets FULL OUTER JOIN moons ON planets.planet_id = moons.planet_id;

/*
planet_id |  name   | moon_id | planet_id |    name
-----------+---------+---------+-----------+------------
         1 | Mercury |         |           |
         2 | Mars    |         |           |
         3 | Earth   |         |           |
         4 | Venus   |         |           |
         5 | Jupiter |       8 |         5 | Thebe
         5 | Jupiter |       7 |         5 | Io
         5 | Jupiter |       6 |         5 | Ganymede
         5 | Jupiter |       5 |         5 | Europa
         5 | Jupiter |       4 |         5 | Carpo
         5 | Jupiter |       3 |         5 | Callisto
         5 | Jupiter |       2 |         5 | Amalthea
         5 | Jupiter |       1 |         5 | Adrastea
         6 | Saturn  |      26 |         6 | Ymir
         6 | Saturn  |      25 |         6 | Tethys
         6 | Saturn  |      24 |         6 | Rhea
         6 | Saturn  |      23 |         6 | Prometheus
         6 | Saturn  |      22 |         6 | Phoebe
         6 | Saturn  |      21 |         6 | Pandora
         6 | Saturn  |      20 |         6 | Pan
         6 | Saturn  |      19 |         6 | Mimas
         6 | Saturn  |      18 |         6 | Janus
         6 | Saturn  |      17 |         6 | Iapetus
         6 | Saturn  |      16 |         6 | Hyperion
         6 | Saturn  |      15 |         6 | Helene
         6 | Saturn  |      14 |         6 | Fenrir
         6 | Saturn  |      13 |         6 | Enceladus
         6 | Saturn  |      12 |         6 | Dione
         6 | Saturn  |      11 |         6 | Daphne
         6 | Saturn  |      10 |         6 | Calypso
         6 | Saturn  |       9 |         6 | Atlas
         7 | Neptune |         |           |
*/

/*
 * Left Join
 * http://www.w3schools.com/Sql/sql_join_full.asp
 * 'LEFT JOIN' keyword returns all rows from the left table (table1),
 *     with the matching rows in the right table (table2).
 *     The result is NULL in the right side when there is no match.
 */
