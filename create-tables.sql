
/* CREATE TABLE IF NOT EXISTS playground (
    equip_id serial PRIMARY KEY,
    type varchar (50) NOT NULL,
    color varchar (25) NOT NULL,
    location varchar(25) check (location in ('north', 'south', 'west', 'east', 'northeast', 'southeast', 'southwest', 'northwest')),
    install_date date
); */


CREATE TABLE IF NOT EXISTS planets (
  planet_id SERIAL PRIMARY KEY,
  name varchar (255)
);


/* Reference to parent planet_id for each moon */
CREATE TABLE IF NOT EXISTS moons (
  moon_id SERIAL PRIMARY KEY,
  planet_id INTEGER REFERENCES planets,
  name varchar (255)
);

CREATE TABLE IF NOT EXISTS moon_characteristics (
  characteristic_id SERIAL PRIMARY KEY,
  moon_id INTEGER REFERENCES moons,
  mass BIGINT,
  diameter INTEGER,
  surface_temperature INTEGER,
  orbit_distance BIGINT,
  orbit_length INT,
  discovery_date DATE
);

COMMENT ON COLUMN moon_characteristics.mass IS 'unit is tonns,  ie 1 megagram, 1000 kilograms';
COMMENT ON COLUMN moon_characteristics.diameter IS 'unit is kilometers';
COMMENT ON COLUMN moon_characteristics.surface_temperature IS 'unit is celsius';
COMMENT ON COLUMN moon_characteristics.orbit_distance IS 'unit is kilometers';
COMMENT ON COLUMN moon_characteristics.orbit_length IS 'unit is earth days';
