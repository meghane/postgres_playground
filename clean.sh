#!/bin/bash

USER=postgres;
sudo -u $USER -H bash -c "psql -f drop-tables.sql";


#
# Dropping table planets with CASCADE option will destroy moon <-> planet constraint
# However the moon table will continue to exist, with orphaned planet_id FK.
#
