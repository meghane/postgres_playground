#!/bin/bash

USER=postgres;

sudo -u $USER -H bash -c "psql -f create-tables.sql";

#
# heredoc it:
# see http://www.tldp.org/LDP/abs/html/here-docs.html for reference
# the db conn & tx will occur within heredoc output fed to the psql app.
#

sudo -u $USER -H bash -c "

psql <<POPULATE

BEGIN;
\i MilkyWay/insert_planets.sql
\i MilkyWay/insert_moons.sql
COMMIT;

POPULATE

"; # end piped -c

exit;
